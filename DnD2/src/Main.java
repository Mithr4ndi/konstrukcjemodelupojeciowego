import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Set;

public class Main {

	public static void main(String[] args) throws Exception{

	
		Gracz gracz1 = new Gracz("Jan","Kowalski", "Pikus", new GregorianCalendar(1995, 2, 11), null );
		Gracz gracz2 = new Gracz("Anna","Malina", "Malinka", new GregorianCalendar(1990, 4, 10), null );
		Gracz gracz3 = new Gracz("Stefan","Krawczyk", "Krawiec", new GregorianCalendar(1992, 5, 9), null );
		
		

		Przygoda przygoda1 = new Przygoda( "Bitwa o Undercity", Przygoda.PoziomTrudnosci.LATWY, 4, 2, null);
		Przygoda przygoda2 = new Przygoda( "Klatwa Ravenlorda", Przygoda.PoziomTrudnosci.LATWY, 4, 2, null);
		Lokacja lokacja1 = new Lokacja("Icecrown", 75, null);
	
		//ASOCJACJA BINARNA
		System.out.println("-------------------ASOCJACJA BINARNA----------------------");
		przygoda1.dodajLokacje(lokacja1);
		przygoda1.pokazLokacje();
		lokacja1.pokazPrzygode();
		System.out.println();
		
		//KOMPOZYCJA
		System.out.println("--------------------KOMPOZYCJA------------------------------");
		Postac.utworzPostac(gracz2, "Nexo", Postac.Klasa.DEATH_KNIGHT, Postac.Rasa.NIGHT_ELF);
		Postac.utworzPostac(gracz3, "Kaileen", Postac.Klasa.PRIEST, Postac.Rasa.DRAENEI);
		gracz2.pokazListePostaci();
		System.out.println();
		
		//ASOCJACJA KWALIFIKOWANA
		System.out.println("-------------------ASOCJACJA KWALIFIKOWANA------------------");
		ArrayList<DnDEkstensja> postaci = Postac.getEkstensja(Postac.class);
		postaci.forEach((node)->{
			Postac postac = (Postac)node;
			System.out.println(postac.toString());
		});
		Postac postac1 = (Postac) postaci.get(0);
		Postac postac2 = (Postac) postaci.get(1);
		Przedmiot przedmiot1 = new Przedmiot("Frostmourne", 100, Przedmiot.Rodzaj.BROŃ, Przedmiot.Rzadkosc.LEGENDARNY, true);
		
		System.out.println("---------------------------------------------------------");
		System.out.println("Testujemy dodawanie graczowi przedmiotu: ");
		postac1.dodajPrzedmiot(przedmiot1);
		postac1.pokazPrzedmioty();
		System.out.println("Testujemy usuwanie graczowi przedmiotu : ");
		postac1.usunPrzedmiot(przedmiot1);
		postac1.pokazPrzedmioty();
		System.out.println("Testujemy przekazywanie przedmiotu: ");
		postac1.dodajPrzedmiot(przedmiot1);
		postac1.pokazPrzedmioty();
		postac2.dodajPrzedmiot(przedmiot1);
		postac1.pokazPrzedmioty();
		postac2.pokazPrzedmioty();
		System.out.println();
		
		
		//ASOCJACJA Z ATRYBUTEM
		System.out.println("----------------ASOCJACJA Z ATRYBUTEM---------------");
		System.out.println("Gracz3 rozpoczyna przygode: ");
		gracz3.rozpocznijPrzygode(przygoda1);
		gracz3.rozpocznijPrzygode(przygoda2);
		gracz3.pokazRozpoczetePrzygody();
		
		System.out.println("Gracz1 rozpoczyna przygode: ");
		gracz1.rozpocznijPrzygode(przygoda1);
		gracz1.rozpocznijPrzygode(przygoda1);
		gracz1.pokazRozpoczetePrzygody();
		
		przygoda1.pokazGraczy();
		
		System.out.println("Gracz3 konczy przygode: ");
		gracz3.zakonczPrzygode(przygoda1);
		gracz3.pokazZakonczonePrzygody();
		
		
		
		gracz3.usun();

			
//			DnDEkstensja.zapiszEkstensje(new ObjectOutputStream(new FileOutputStream("DnDEkstensja.txt")));
//			DnDEkstensja.odczytajEkstensje(new ObjectInputStream(new FileInputStream("DnDEkstensja.txt")));
//			Gracz.pokazEkstensje(Gracz.class);
//			System.out.println();
//			
//			ArrayList<DnDEkstensja> lista = DnDEkstensja.getEkstensja(Gracz.class);
//			DnDEkstensja.UsunZEkstensji(lista.get(0));
//			
//			Gracz.pokazEkstensje(Gracz.class);
//			System.out.println();

	}

	static String[] nazwyPrzygod = { "Bitwa o Undercity", "Upadek Angratharu", "Klatwa Ravenlorda", "Plonaca Krucjata",
			"Ucieczka z Durnholde", "Powrot do Teldrassil", "Odrodzone Przymierze", "Pani Lodowej Cytadeli",
			"Ostatnia Jask�ka", "Kwiat Wisielcow", "Ogrod smierci", "Krew smoka" };
}
