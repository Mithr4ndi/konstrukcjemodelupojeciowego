import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;

public class Gracz extends Osoba {

	private static final long serialVersionUID = 6624732662552104349L;

	// Kompozycja - calosc
	private ArrayList<Postac> listaPostaci = new ArrayList<>();
	private static HashSet<Postac> wszystkiePostaci = new HashSet<>();

	private ArrayList<GraczPrzygoda> rozpoczetePrzygody = new ArrayList<>();
	private ArrayList<GraczPrzygoda> zakonczonePrzygody = new ArrayList<>();

	public Gracz(String imie, String nazwisko, String nick, GregorianCalendar gregorianCalendar, String plec)
			throws Exception {
		super(imie, nazwisko, nick, plec, gregorianCalendar);
	}

	public void pokazRozpoczetePrzygody() {
		System.out.println("Rozpoczete przygody gracza " + getNick() + ": ");
		for (GraczPrzygoda gP : rozpoczetePrzygody) {
			System.out.println(gP);
		}
	}

	public void pokazZakonczonePrzygody() {
		System.out.println("Zakonczone przygody gracza " + getNick() + ": ");
		for (GraczPrzygoda gP : zakonczonePrzygody) {
			System.out.println(gP);
		}
	}

	// ------------------ METODY KOMPOZYCJI ----------------------------------------

	public void dodajPostac(Postac postac) throws Exception {
		if (!listaPostaci.contains(postac)) {
			if (wszystkiePostaci.contains(postac)) {
				throw new Exception("Ta postac juz nalezy do innego gracza!");
			}
			listaPostaci.add(postac);
			wszystkiePostaci.add(postac);
		}
	}

	public void pokazListePostaci() {
		System.out.printf("Lista postaci gracza %s :\n", nick);
		for (Postac p : listaPostaci) {
			System.out.println("\t *" + p.getNazwa() + " " + p.getKlasaPostaci() + " " + p.getRasa() + " ("
					+ p.getGracz().nick + ")");
		}
		System.out.println();
	}

	// ------------------ METODY DO ASOCJACJI Z ATRYBUTEM-------------------------
	public void rozpocznijPrzygode(Przygoda przygoda) {
		GraczPrzygoda gP = null;
		if(!GraczPrzygoda.czyIstnieje(this, przygoda)) {
			GraczPrzygoda.utworzGraczPrzygoda(this, przygoda);
		}
	}
	
	public void rozpocznijPrzygode(GraczPrzygoda gP) {
		if(!rozpoczetePrzygody.contains(gP)) {
			rozpoczetePrzygody.add(gP);
		}
	}

	public void zakonczPrzygode(Przygoda przygoda) {
		GraczPrzygoda result = null;
		for(GraczPrzygoda gP : rozpoczetePrzygody) {
			if(gP.getPrzygoda().equals(przygoda)) {
				result = gP;
			}
		}
		if (result != null) {
			
			result.aktualizujGraczPrzygoda();
			rozpoczetePrzygody.remove(result);
			if (!zakonczonePrzygody.contains(result)) {
				this.zakonczonePrzygody.add(result);
			}
		}
	}

	public ArrayList<GraczPrzygoda> getRozpoczetePrzygody() {
		return (ArrayList<GraczPrzygoda>) this.rozpoczetePrzygody.clone();
	}

	public ArrayList<GraczPrzygoda> getZakonczonePrzygody() {
		return (ArrayList<GraczPrzygoda>) this.zakonczonePrzygody.clone();
	}

	@Override
	public void usun() {
		UsunZEkstensji();
		for(Postac p : wszystkiePostaci) {
			if(p.getGracz().equals(this)) {
			
				p.usun();
			}
		}
		System.out.println("Gracz: " + getNick() + " zostal usuniety");
	}

}
