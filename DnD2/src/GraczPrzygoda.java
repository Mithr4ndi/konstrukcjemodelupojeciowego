import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class GraczPrzygoda extends DnDEkstensja {

	private Gracz gracz;
	private Przygoda przygoda;

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
	private Calendar dataRozpoczecia;
	private Calendar dataZakonczenia = null;

	public GraczPrzygoda(Gracz gracz, Przygoda przygoda) {
		super();
		this.gracz = gracz;
		this.przygoda = przygoda;
		this.dataRozpoczecia = new GregorianCalendar();
	}

	// ------------------ METODY DO ASOCJACJI Z ATRYBUTEM --------------------

	public static void utworzGraczPrzygoda(Gracz gracz, Przygoda przygoda) {
		GraczPrzygoda graczPrzygoda = null;
//		try {
//			for (DnDEkstensja dnd : GraczPrzygoda.getEkstensja(GraczPrzygoda.class)) {
//				if (gracz.equals(graczPrzygoda.getGracz()) && przygoda.equals(graczPrzygoda.getPrzygoda())) {
//					break;
//				}
//			}
//		} catch (Exception e) {
//		}
		
		graczPrzygoda = new GraczPrzygoda(gracz, przygoda);
	
		przygoda.dodajGracza(graczPrzygoda);
		gracz.rozpocznijPrzygode(graczPrzygoda);
	}

	public void aktualizujGraczPrzygoda() {

		dataZakonczenia = new GregorianCalendar();

	}
	
	
	public static boolean czyIstnieje(Gracz gracz, Przygoda przygoda) {
		boolean result = false;
		try {
			ArrayList<DnDEkstensja> ekstensja = GraczPrzygoda.getEkstensja(GraczPrzygoda.class);
			for(DnDEkstensja dnd : ekstensja) {
				GraczPrzygoda gP = (GraczPrzygoda)dnd;
				if(gP.getGracz().equals(gracz) && gP.getPrzygoda().equals(przygoda)) {
					result = true;
				}
			}
		} catch (Exception e) {
		}
		
		return result;
	}

	// ------------------ OVERRIDE --------------------------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Historia rozgrywki zostala usunieta");
	}

	@Override
	public String toString() {
		String zakonczeniePrzygody = dataZakonczenia == null ? "Gracz jeszcze nie zakonczyl przygody"
				: getDataZakonczenia();
		return przygoda.getTytul() + "\nPoczatek: " + getDataRozpoczecia() + " Koniec: " + zakonczeniePrzygody + "\n";
	}

	// ------------------ GETTERS & SETTERS---------------------------------------

	public String getDataZakonczenia() {
		return (dateFormat.format(dataZakonczenia.getTime()));
	}

	public void setDataZakonczenia(Calendar dataZakonczenia) {
		this.dataZakonczenia = dataZakonczenia;
	}

	public String getDataRozpoczecia() {
		return (dateFormat.format(dataRozpoczecia.getTime()));
	}

	public Gracz getGracz() {
		return gracz;
	}

	public Przygoda getPrzygoda() {
		return przygoda;
	}

}
