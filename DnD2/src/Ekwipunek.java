import java.util.ArrayList;

public class Ekwipunek  extends DnDEkstensja {

	
	private String nazwaPlecaka;
	private int pojemnosc;
	private ArrayList<String> listaPrzedmiotow = new ArrayList<>();
	
	
	public Ekwipunek(String nazwaPlecaka, int pojemnosc) throws Exception {
		super();
		sprawdzCzyJestPusty(nazwaPlecaka, "Nazwa plecka jest wymagana!");
		this.nazwaPlecaka = nazwaPlecaka;
		
		sprawdzCzyJestPusty(pojemnosc, "Pojemnosc jest wymagana!");
		this.pojemnosc = pojemnosc;
	}
	
	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Ekwipunek: " + getNazwaPlecaka() + " zostal usuniety");	
	}
	
	
	@Override
	public String toString() {
		return "Ekwipunek:\n" + nazwaPlecaka + " " + pojemnosc;
	}
	
	
	public void dodajPrzedmiot(String przedmiot) {
		listaPrzedmiotow.add(przedmiot);
		System.out.println("Przedmiot " + przedmiot + " zostal dodany do ekwpinuku.");
	}
	
	public void usunPrzedmiot(String przedmiot) {
		int index = listaPrzedmiotow.indexOf(przedmiot);
		listaPrzedmiotow.remove(index);
		System.out.println("Przedmiot " +  przedmiot + " zostal usuniety z ekwipunku.");
	}
	
	public void pokazEkwipunek() {
		for (String str : listaPrzedmiotow) 
			System.out.println(str);
	}
	
	
	// ------------------ GETTERS & SETTERS---------------------------
	
	
	public String getNazwaPlecaka() {
		return nazwaPlecaka;
	}


	public void setNazwaPlecaka(String nazwaPlecaka) {
		this.nazwaPlecaka = nazwaPlecaka;
	}


	public int getPojemnosc() {
		return pojemnosc;
	}


	public void setPojemnosc(int pojemnosc) {
		this.pojemnosc = pojemnosc;
	}


	public ArrayList<String> getListaPrzedmiotow() {
		ArrayList<String> tmpListaPrzedmiotow = new ArrayList<>();
		for (String str : listaPrzedmiotow) {
			tmpListaPrzedmiotow.add(str);
		}
		
		return tmpListaPrzedmiotow;
	}
}
