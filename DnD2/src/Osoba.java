import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class Osoba extends DnDEkstensja{


	private static final long serialVersionUID = -2831920764602060574L;
	
	protected String imie;
	protected String nazwisko;
	protected String nick;
	protected String plec;  //Atrybut opcjonalny
	protected Calendar dataUrodzenia; //Atrybut zlozony

	public Osoba(String imie, String nazwisko, String nick, String plec, GregorianCalendar gregorianCalendar) throws Exception {
		super();
		this.plec = plec;
		inicjuj(imie, nazwisko, nick, gregorianCalendar);

	}

	private void inicjuj(String imie, String nazwisko, String nick, GregorianCalendar dataUrodzenia) throws Exception {
	
		sprawdzCzyJestPusty(imie,"Imie jest wymagane!");
		this.imie = imie;
		sprawdzCzyJestPusty(nazwisko,"Nazwisko jest wymagane!");
		this.nazwisko = nazwisko;
		sprawdzCzyJestPusty(nick,"Nick jest wymagany!");
		this.nick = nick;
		sprawdzCzyJestPusty(dataUrodzenia, "Data urodzenia jest wymagana!");
		this.dataUrodzenia = dataUrodzenia;
		
		if(plec == null) {
			plec = "Nieznana";
		}
	}

	//Atrybut wyliczalny
	public int getWiek() {
		return (Year.now().getValue() - dataUrodzenia.get(Calendar.YEAR));
	}

	@Override
	public String toString() {
		return nick + ": " + imie + " " + nazwisko + " " + getDataUrodzenia() + " Plec: " + plec;
		
	}
	
	
	//------------------ GETTERS & SETTERS---------------------------

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getDataUrodzenia() {
		return (dataUrodzenia.get(Calendar.DAY_OF_MONTH) +"/"+ dataUrodzenia.get(Calendar.MONTH)+"/" + dataUrodzenia.get(Calendar.YEAR));
	}

	public void setDataUrodzenia(Calendar dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

}
