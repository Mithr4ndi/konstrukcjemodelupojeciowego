# KonstrukcjeModeluPojeciowego

Repozytorium zawiera różne konstrukcje modelu pojęciowego.

DnD zawiera:
-Realizację trwałości ekstensji za pomocą serializacji
-Atrybut złożony
-Atrybut opcjonalny
-Atrybut powtarzalny
-Atrybut klasowy
-Atrybut pochodny
-Atrybut klasowa
-Przesłonięcie,
przeciążenie

DnD2 zawiera:
-Realizację następujących asocjacji:
• Binarna
• Z atrybutem
• Kwalifikowana
• Kompozycja
(w każdym przypadku: liczności
1-* lub *-* oraz automatyczne tworzenie połączenia zwrotnego)

DnD3 zawiera:
-Realizację następujących dziedziczeń:
• Klasa abstrrakcyjna/ polimorfizm
• Overlapping
• Wielodziedziczenie
• Wieloaspektowe
• Dynamiczne
