package DnDTest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.io.ObjectOutputStream;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Set;

public class Main {

	public static void main(String[] args) throws Exception {

		System.out.println("------------------------------------DYNAMIC-----------------------------");
		//testDynamic();

		System.out.println("------------------------------------OVERLAPPING--------------------------");
		//testOverlapping();

		System.out.println("-----------------------------WIELOASPEKTOWE-------------------------------");
		testWieloaspektowe();

		System.out.println("------------------------------------WIELOKROTNE---------------------------");
		//testWielokrotne();
	}

	private static void testWielokrotne() throws Exception {
		Wielokrotne.GrajacyMistrzGry grajacyMG = new Wielokrotne.GrajacyMistrzGry("Jan", "Kowalski", "Pikus", null,
				new GregorianCalendar(1995, 2, 11), Wielokrotne.Gracz.Znak.LORD,
				Wielokrotne.MistrzGry.Specjalizacja.MISTRZ_LEGEND);

		System.out.println(grajacyMG);

	}

	private static void testOverlapping() throws Exception {
		Overlapping.Osoba o = new Overlapping.Osoba("Jan", "Kowalski", "Pikus", null,
				new GregorianCalendar(1995, 2, 11), Overlapping.Gracz.Znak.LORD,
				Overlapping.MistrzGry.Specjalizacja.MISTRZ_LEGEND, Overlapping.Osoba.TypOsoby.GRACZ_MISTRZGRY);
		System.out.println(o);
	}

	private static void testWieloaspektowe() throws Exception {
		Wieloaspektowe.Przedmiot pancerz = new Wieloaspektowe.Pancerz("Helm legionisty", 45,
				Wieloaspektowe.Przedmiot.Rzadkosc.RZADKI, 15, 100, Wieloaspektowe.Pancerz.RodzajPancerza.HELM,
				Wieloaspektowe.Pancerz.TypPancerza.PLYTOWY);
		Wieloaspektowe.Przedmiot bron1 = new Wieloaspektowe.Bron("Thunderfury", 80,
				Wieloaspektowe.Przedmiot.Rzadkosc.POSPOLIT, 300, 10, 30, Wieloaspektowe.Bron.RodzajBroni.MIECZ, null, "Co czwarty cios powoduje ogluszenie przeciwnika na 2s");
		Wieloaspektowe.Przedmiot bron2 = new Wieloaspektowe.Bron("Frostmourne", 100,
				Wieloaspektowe.Przedmiot.Rzadkosc.LEGENDARNY, 1000, 25, 200, Wieloaspektowe.Bron.RodzajBroni.MIECZ, 300, "Wykrada dusze ofiary i tym samym gracz zyskuje niesmiertelnosc raz na 1 ture przez 12s");
	
		System.out.println(pancerz);
		System.out.println(bron1);
		System.out.println(bron2);
		
		System.out.println(pancerz.getCalkowiteStatystyki());
		System.out.println(bron1.getCalkowiteStatystyki());
	
	}

	private static void testDynamic() throws Exception {
		// Utworzenie obiektu Gracz
		Dynamic.Osoba gracz1 = new Dynamic.Gracz("Jan", "Kowalski", "Pikus", null, new GregorianCalendar(1995, 2, 11),
				Dynamic.Gracz.Znak.LORD);
		System.out.println(gracz1);
		// Zmiana obiektu na MistrzGry
		gracz1 = new Dynamic.MistrzGry(gracz1, Dynamic.MistrzGry.Specjalizacja.MISTRZ_LEGEND);
		System.out.println(gracz1);

	}

}
