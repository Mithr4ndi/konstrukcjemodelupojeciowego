package Wieloaspektowe;


import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public abstract class Osoba extends DnDEkstensja {

	private static final long serialVersionUID = -2831920764602060574L;

	protected String imie;
	protected String nazwisko;
	protected String nick;
	protected String plec;
	protected GregorianCalendar dataUrodzenia;

	public Osoba(String imie, String nazwisko, String nick, String plec, GregorianCalendar dataUrodzenia)
			throws Exception {
		super();
		inicjuj(imie, nazwisko, nick, plec, dataUrodzenia);
	}

	private void inicjuj(String imie, String nazwisko, String nick, String plec, GregorianCalendar dataUrodzenia)
			throws Exception {

		sprawdzCzyJestPusty(imie, "Imie jest wymagane!");
		this.imie = imie;
		sprawdzCzyJestPusty(nazwisko, "Nazwisko jest wymagane!");
		this.nazwisko = nazwisko;
		sprawdzCzyJestPusty(nick, "Nick jest wymagany!");
		this.nick = nick;
		sprawdzCzyJestPusty(dataUrodzenia, "Data urodzenia jest wymagana!");
		this.dataUrodzenia = dataUrodzenia;

		if (this.plec == null) {
			this.plec = "Nieznana";
		} else
			this.plec = plec;
	}

	// Atrybut wyliczalny
	public int getWiek() {
		return (Year.now().getValue() - dataUrodzenia.get(Calendar.YEAR));
	}

	public abstract String getWlasciwosc();

	// ------------------ OVERRIDE -----------t---------------------------

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + nick + ": " + imie + " " + nazwisko + " " + getDataUrodzenia()
				+ " Plec: " + plec + "\nWlasciwosc: " + getWlasciwosc() + "\n";

	}

	// ------------------ GETTERS & SETTERS---------------------------

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getDataUrodzenia() {
		return (dataUrodzenia.get(Calendar.DAY_OF_MONTH) + "/" + dataUrodzenia.get(Calendar.MONTH) + "/"
				+ dataUrodzenia.get(Calendar.YEAR));
	}

	public void setDataUrodzenia(GregorianCalendar dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

}
