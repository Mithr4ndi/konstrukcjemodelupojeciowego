package Wieloaspektowe;
import java.util.Map;
import java.util.TreeMap;

public abstract class Przedmiot extends DnDEkstensja {

	public enum Rzadkosc {
		POSPOLIT, RZADKI, BARDZO_RZADKI, LEGENDARNY
	}


	private String nazwaPrzedmiotu;
	private int poziomPrzedmiotu;
	private Rzadkosc rzadkoscPrzedmiotu;

	private Postac postac;
	private Moc moc = PrzedmiotNiemagiczny.dodajPrzedmiotNiemagiczny(this, "Brak");



	
	public Przedmiot(String nazwaPrzedmiotu, int poziomPrzedmiotu, Rzadkosc rzadkoscPrzedmiotu) throws Exception {
		super();
		sprawdzCzyJestPusty(nazwaPrzedmiotu, "Nazwa przedmiotu jest wymagana!");
		this.nazwaPrzedmiotu = nazwaPrzedmiotu;
		
		sprawdzCzyJestPusty(poziomPrzedmiotu, "Poziom przedmiotu jest wymagany!");
		this.poziomPrzedmiotu = poziomPrzedmiotu;
		
		sprawdzCzyJestPusty(rzadkoscPrzedmiotu, "Rzadkosc przedmiotu jest wymagana!");
		this.rzadkoscPrzedmiotu = rzadkoscPrzedmiotu;
	
	}
	
	
	public abstract String getCalkowiteStatystyki();
	
	
	// ------------------ METODY KOMPOZYCJI ----------------------------------------

	public Moc getMoc() {
		return moc;
	}

	public void setMoc(Moc moc) {
		if (this.moc != moc) {
			this.moc = moc;
			if (moc.getPrzedmiot() == null) {
				moc.setPrzedmiot(this);
			}
		}
	}



	// ------------------ METODY ASOCJACJI KWALIFIKOWANEJ ---------------------------

	public Postac getPostac(){
		return this.postac;
	}
	

	public void setNazwaPrzedmiotu(String nazwaPrzedmiotu) {
		this.nazwaPrzedmiotu = nazwaPrzedmiotu;
	}


	public void setPostac(Postac postac) {
		if(this.postac == null){
			postac.dodajPrzedmiot(this);
			this.postac = postac;
		}
		else if(postac == null){
			this.postac.usunPrzedmiot(this);
			this.postac = null;
		}
		else if(this.postac != postac){
			this.postac.usunPrzedmiot(this);
			postac.dodajPrzedmiot(this);
			this.postac = postac;
		}
	}
	
	
	// ------------------ OVERRIDE ---------------------------------------------------
	
	@Override
	public void usun() {
		UsunZEkstensji();
		
		System.out.println("Przedmiot: " + getNazwaPrzedmiotu() + " zostal usuniety.");
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName()+" " + nazwaPrzedmiotu + "\nPoziom: " + poziomPrzedmiotu + " " + getMoc() + "\n";	
	}
	
	
	// ------------------ GETTERS & SETTERS---------------------------
	
	public String getNazwaPrzedmiotu() {
		return nazwaPrzedmiotu;
	}

	public void setNazwa(String nazwaPrzedmiotu) {
		this.nazwaPrzedmiotu = nazwaPrzedmiotu;
	}

	public int getPoziomPrzedmiotu() {
		return poziomPrzedmiotu;
	}

	public void setPoziomPrzedmiotu(int poziomPrzedmiotu) {
		this.poziomPrzedmiotu = poziomPrzedmiotu;
	}

	public Rzadkosc getRzadkoscPrzedmiotu() {
		return rzadkoscPrzedmiotu;
	}

	public void setRzadkoscPrzedmiotu(Rzadkosc rzadkoscPrzedmiotu) {
		this.rzadkoscPrzedmiotu = rzadkoscPrzedmiotu;
	}
	
	

}
