package Wieloaspektowe;

import Overlapping.MistrzGry;
import Wieloaspektowe.Przedmiot.Rzadkosc;

public abstract class Moc extends DnDEkstensja {
	
	private Przedmiot przedmiot;
	
	private String bonus;
	
	public Moc(Przedmiot przedmiot, String bonus) {
		super();
		this.przedmiot = przedmiot;
		przedmiot.setMoc(this);
		this.bonus = bonus;
	}
	
	public Przedmiot getPrzedmiot() {
		return przedmiot;
	}

	public void setPrzedmiot(Przedmiot przedmiot) {
		this.przedmiot = przedmiot;
	}

	public String getBonus() {
		return bonus;
	}

	public void setBonus(String bonus) {
		this.bonus = bonus;
	}
	
	
	
}
