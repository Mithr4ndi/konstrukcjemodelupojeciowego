package Wieloaspektowe;

public class PrzedmiotNiemagiczny extends Moc {

	private String bonusFizyczny;

	public PrzedmiotNiemagiczny(Przedmiot przedmiot, String bonus) {
		super(przedmiot, bonus);
		this.bonusFizyczny = bonus;
	}

	public static Moc dodajPrzedmiotNiemagiczny(Przedmiot przedmiot, String bonus) {
		if (przedmiot == null) {
			return null;
		}
		PrzedmiotNiemagiczny pNiemagiczny = new PrzedmiotNiemagiczny(przedmiot, bonus);
		return pNiemagiczny;
	}

	public String getBonusFizyczny() {
		return bonusFizyczny;
	}

	public void setBonusFizyczny(String bonusFizyczny) {
		this.bonusFizyczny = bonusFizyczny;
	}

	@Override
	public String toString() {
		return "Przedmiot niemagiczny: " + getPrzedmiot().getNazwaPrzedmiotu() + "\nBonus Fizyczny: " + getBonusFizyczny();
	}

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Przedmiot niemagiczny: " + getPrzedmiot().getNazwaPrzedmiotu() + " zostal usuniety!");

	}

}
