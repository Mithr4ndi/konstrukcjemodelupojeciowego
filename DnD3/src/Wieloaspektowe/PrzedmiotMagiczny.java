package Wieloaspektowe;


public class PrzedmiotMagiczny extends Moc {

	private Integer inteligencja;
	private String bonusMagiczny;

	public PrzedmiotMagiczny(Przedmiot przedmiot, String bonus, Integer inteligencja) {
		super(przedmiot, bonus);
		this.bonusMagiczny = bonus;
		this.inteligencja = inteligencja;
	}

	public static Moc dodajPrzedmiotMagiczny(Przedmiot przedmiot, String bonus, Integer inteligencja) {
		if (przedmiot == null) {
			return null;
		}
		PrzedmiotMagiczny pMagiczny = new PrzedmiotMagiczny(przedmiot, bonus, inteligencja);
		return pMagiczny;
	}

	public Integer getInteligencja() {
		return inteligencja;
	}

	public void setInteligencja(Integer inteligencja) {
		this.inteligencja = inteligencja;
	}

	public String getBonusMagiczny() {
		return bonusMagiczny;
	}

	public void setBonusMagiczny(String bonusMagiczny) {
		this.bonusMagiczny = bonusMagiczny;
	}

	@Override
	public String toString() {
		return "Przedmiot magiczny: " + getPrzedmiot().getNazwaPrzedmiotu() + "\nInteligencja: " + getInteligencja()
				+ "\nBonus Magiczny: " + getBonusMagiczny();
	}

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Przedmiot magiczny: " + getPrzedmiot().getNazwaPrzedmiotu() + " zostal usuniety!");
	}

}
