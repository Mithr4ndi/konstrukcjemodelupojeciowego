package Wieloaspektowe;


public class Bron extends Przedmiot{

	public enum RodzajBroni{
			MIECZ,
			TOPOR,
			SZTYLET,
			WLOCZNIA,
			LUK,
			KUSZA,
			LASKA
	}
	
	private int podstawoweObrazenia;
	private int predkosc;
	private int silaAtaku;
	private RodzajBroni rodzajBroni;
	
	public Bron(String nazwaPrzedmiotu, int poziomPrzedmiotu, Rzadkosc rzadkoscPrzedmiotu, int podstawoweObrazenia,
			int predkosc, int silaAtaku, RodzajBroni rodzajBroni)
			throws Exception {
		super(nazwaPrzedmiotu, poziomPrzedmiotu, rzadkoscPrzedmiotu);
		
		sprawdzCzyJestPusty(podstawoweObrazenia, "Podstawowe obrazenia sa wymagame!");
		this.podstawoweObrazenia = podstawoweObrazenia;
		sprawdzCzyJestPusty(predkosc, "Predkosc jest wymagana!");
		this.predkosc = predkosc;
		sprawdzCzyJestPusty(silaAtaku, "Sila ataku jest wymagana!");
		this.silaAtaku = silaAtaku;
		sprawdzCzyJestPusty(rodzajBroni, "Rodzaj broni jest wymagany!");
	}
	
	public Bron(String nazwaPrzedmiotu, int poziomPrzedmiotu, Rzadkosc rzadkoscPrzedmiotu, int podstawoweObrazenia,
			int predkosc, int silaAtaku, RodzajBroni rodzajBroni, Integer inteligencja, String bonus)
			throws Exception {
		super(nazwaPrzedmiotu, poziomPrzedmiotu, rzadkoscPrzedmiotu);
		
		sprawdzCzyJestPusty(podstawoweObrazenia, "Podstawowe obrazenia sa wymagame!");
		this.podstawoweObrazenia = podstawoweObrazenia;
		sprawdzCzyJestPusty(predkosc, "Predkosc jest wymagana!");
		this.predkosc = predkosc;
		sprawdzCzyJestPusty(silaAtaku, "Sila ataku jest wymagana!");
		this.silaAtaku = silaAtaku;
		sprawdzCzyJestPusty(rodzajBroni, "Rodzaj broni jest wymagany!");
		
		if(inteligencja != null) 
			setMoc(PrzedmiotMagiczny.dodajPrzedmiotMagiczny(this, bonus, inteligencja));
		 else
			setMoc(PrzedmiotNiemagiczny.dodajPrzedmiotNiemagiczny(this, bonus));
	}
	
	
	@Override
	public String getCalkowiteStatystyki() {
		return this.getClass().getSimpleName() + " " + this.getNazwaPrzedmiotu() + " zadaje " + getCalkowiteOBrazenia() + " calkowitych obrazen." ;
	}
	
	public int getCalkowiteOBrazenia() {
		return (int) (podstawoweObrazenia + (silaAtaku * 0.3 + silaAtaku));
	}
	
	
	public RodzajBroni getRodzajBroni() {
		return rodzajBroni;
	}


	public void setRodzajBroni(RodzajBroni rodzajBroni) {
		this.rodzajBroni = rodzajBroni;
	}
	
	public int getPodstawoweObrazenia() {
		return podstawoweObrazenia;
	}

	public void setPodstawoweObrazenia(int podstawoweObrazenia) {
		this.podstawoweObrazenia = podstawoweObrazenia;
	}

	public int getPredkosc() {
		return predkosc;
	}

	public void setPredkosc(int predkosc) {
		this.predkosc = predkosc;
	}

	public int getSilaAtaku() {
		return silaAtaku;
	}

	public void setSilaAtaku(int silaAtaku) {
		this.silaAtaku = silaAtaku;
	}

	
	
	

	
}
