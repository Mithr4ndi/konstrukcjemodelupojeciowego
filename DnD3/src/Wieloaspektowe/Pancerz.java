package Wieloaspektowe;


public class Pancerz extends Przedmiot {

	public enum RodzajPancerza {
		HELM, NAPIERSNIK, KARWASZE, REKAWICE, NARAMIENNKI, BUTY, NAGOLENNIKI
	}

	public enum TypPancerza {
		PLYTOWY, SKORZANY, PLOCIENNY, KOLCZUGA
	}

	private int podstawowePunktyOBrony;
	private int wytrzymaloscPancerza;
	private TypPancerza typPancerza;
	private RodzajPancerza rodzajPancerza;

	public Pancerz(String nazwaPrzedmiotu, int poziomPrzedmiotu, Rzadkosc rzadkoscPrzedmiotu,
			int podstawowePunktyObrony, int wytrzymaloscPancerza, RodzajPancerza rodzajPancerza,
			TypPancerza typPancerza) throws Exception {
		super(nazwaPrzedmiotu, poziomPrzedmiotu, rzadkoscPrzedmiotu);
		sprawdzCzyJestPusty(podstawowePunktyObrony, "Podstawowe punkty obrony sa wyamgane!");
		this.podstawowePunktyOBrony = podstawowePunktyObrony;
		sprawdzCzyJestPusty(wytrzymaloscPancerza, "Wytrzymalosc pancerza jest wymagana!");
		this.wytrzymaloscPancerza = wytrzymaloscPancerza;
		sprawdzCzyJestPusty(rodzajPancerza, "Rodzaj pancerza jest wymagany!");
		this.rodzajPancerza = rodzajPancerza;
		sprawdzCzyJestPusty(typPancerza, "Typ pancerza nie moze byc pusty!");
		this.typPancerza = typPancerza;
	}

	public Pancerz(String nazwaPrzedmiotu, int poziomPrzedmiotu, Rzadkosc rzadkoscPrzedmiotu, int podstawowePunktyObrony, 
			int wytrzymaloscPancerza, RodzajPancerza rodzajPancerza, TypPancerza typPancerza, Integer inteligencja, String bonus) throws Exception {
		super(nazwaPrzedmiotu, poziomPrzedmiotu, rzadkoscPrzedmiotu);
		
		sprawdzCzyJestPusty(podstawowePunktyObrony, "Podstawowe punkty obrony sa wyamgane!");
		this.podstawowePunktyOBrony = podstawowePunktyObrony;
		sprawdzCzyJestPusty(wytrzymaloscPancerza, "Wytrzymalosc pancerza jest wymagana!");
		this.wytrzymaloscPancerza = wytrzymaloscPancerza;
		sprawdzCzyJestPusty(rodzajPancerza, "Rodzaj pancerza jest wymagany!");
		this.rodzajPancerza = rodzajPancerza;
		sprawdzCzyJestPusty(typPancerza, "Typ pancerza nie moze byc pusty!");
		this.typPancerza = typPancerza;
		
		if(inteligencja != null) 
			setMoc(PrzedmiotMagiczny.dodajPrzedmiotMagiczny(this, bonus, inteligencja));
		 else
			setMoc(PrzedmiotNiemagiczny.dodajPrzedmiotNiemagiczny(this, bonus));
	}
	
	
	@Override
	public String getCalkowiteStatystyki() {
		return this.getClass().getSimpleName() + " " + this.getNazwaPrzedmiotu() + " posiada " + getCalkowitePunktyObrony() + " calkowitych punktow obrony.";
	}

	public int getCalkowitePunktyObrony() {
		return (int) (podstawowePunktyOBrony + (wytrzymaloscPancerza * 0.3 + wytrzymaloscPancerza));
	}

	public int getPodstawowePunktyOBrony() {
		return podstawowePunktyOBrony;
	}

	public void setPodstawowePunktyOBrony(int podstawowePunktyOBrony) {
		this.podstawowePunktyOBrony = podstawowePunktyOBrony;
	}

	public int getWytrzymaloscPancerza() {
		return wytrzymaloscPancerza;
	}

	public void setWytrzymaloscPancerza(int wytrzymaloscPancerza) {
		this.wytrzymaloscPancerza = wytrzymaloscPancerza;
	}

	public TypPancerza getTypPancerza() {
		return typPancerza;
	}

	public void setTypPancerza(TypPancerza typPancerza) {
		this.typPancerza = typPancerza;
	}

	public RodzajPancerza getRodzajPancerza() {
		return rodzajPancerza;
	}

	public void setRodzajPancerza(RodzajPancerza rodzajPancerza) {
		this.rodzajPancerza = rodzajPancerza;
	}

	
}
