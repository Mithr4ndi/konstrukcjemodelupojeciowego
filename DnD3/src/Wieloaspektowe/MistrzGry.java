package Wieloaspektowe;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MistrzGry extends Osoba {

	public enum Specjalizacja {
			MISTRZ_LOCHOW,
			MISTRZ_ZYWIOLOW,
			MISTRZ_POTWOROW,
			MISTRZ_LEGEND		
	}
	
	private ArrayList<String> scenariusze = new ArrayList<>();
	private ArrayList<Przygoda> przygody = new ArrayList<>();
	private Specjalizacja specjalizacja;

	public MistrzGry(String imie, String nazwisko, String nick, String plec, GregorianCalendar gregorianCalendar, Specjalizacja spec)
			throws Exception {
		super(imie, nazwisko, nick, plec, gregorianCalendar);
		this.specjalizacja = spec;
	}

	public MistrzGry(Osoba osoba, Specjalizacja spec) throws Exception {
		super(osoba.imie, osoba.nazwisko, osoba.nick, osoba.plec, osoba.dataUrodzenia);
		this.specjalizacja = spec;
		System.out.println(getNick() + " jest teraz Mistrzem Gry!");
	}
	
	
	public ArrayList<String> getScenariusze() {
		return (ArrayList<String>) this.scenariusze.clone();
	}
	
	public ArrayList<Przygoda> getPrzygody() {
		return (ArrayList<Przygoda>) this.przygody.clone();
	}

	public void dodajScenariusz(String scenariusz) throws Exception {
		if (!scenariusze.contains(scenariusz)) {
			scenariusze.add(scenariusz);
			System.out.println("Scenariusz zostal dodany.");
		} else {
			throw new Exception("Taki scenariusz juz istnieje!");
		}
	}

	public void usunScenariusz(String scenariusz) throws Exception {
		if (!scenariusze.contains(scenariusz)) {
			scenariusze.remove(scenariusz);
			System.out.println("Scenariusz zostal usuniety.");
		} else {
			throw new Exception("Taki scenariusz nie istniejekj!");
		}
	}
	
	

	// ------------------ METODY DO ASOCJACJI BINARNEJ -----------------------

	public void dodajPrzygode(Przygoda przygoda) {
		if (!this.przygody.contains(przygoda)) {
			przygody.add(przygoda);
			if (przygoda.getMistrzGry() == null) {
				przygoda.setMistrzGry(this);
			}
		}
	}

	public void usunPrzygode(Przygoda przygoda) {
		if (this.przygody.contains(przygoda)) {
			this.przygody.remove(przygoda);
			if (przygoda.getMistrzGry() != null) {
				przygoda.setMistrzGry(null);
			}
		}
	}

	public void pokazPrzygody() {
		System.out.printf("Przygody stworzone przez Mistrza Gry, \"%s\" : \n",getNick());
		for (Przygoda p : przygody) {
			System.out.println("\t*" + p.getTytul());
		}
		System.out.println();
	}

	// --------------------------OVERRIDE-------------------------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Mistrz Gry " + getNick() + " zostal usuniety!");

	}

	@Override
	public String getWlasciwosc() {
		return specjalizacja.toString();
	}
		
}
