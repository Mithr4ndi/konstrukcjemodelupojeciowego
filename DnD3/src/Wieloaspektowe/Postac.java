package Wieloaspektowe;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Postac extends DnDEkstensja {

	public enum Klasa {
		WARRIOR, DEATH_KNIGHT, PALADIN, PRIEST, WARLOCK, MAGE, ROGUE, DRUID, SHAMAN, HUNTER
	}

	public enum Rasa {
		NIGHT_ELF, ORC, HUMAN, DWARF, TROLL, WORGEN, DRAENEI, BLOOD_ELF, UNDEAD, TAUREN
	}

	private String nazwa;
	private int poziomZdrowia = 100;
	private int poziomMany;
	private int doswiadczenie;
	private int poziom = 0;

	private Klasa klasaPostaci;
	private Rasa rasa;

	// Kompozycja - czesc
	private Gracz graczCalosc;

	// Asocjacja kwalifikowana licznosc *
	private TreeMap<String, Przedmiot> przedmioty = new TreeMap<String, Przedmiot>();

	private Postac(Gracz graczCalosc, String nazwa, Klasa klasaPostaci, Rasa rasa) throws Exception {
		super();

		this.graczCalosc = graczCalosc;

		sprawdzCzyJestPusty(nazwa, "Nazwa jest wymagana!");
		this.nazwa = nazwa;

		sprawdzCzyJestPusty(klasaPostaci, "Klasa jest wymagana!");
		this.klasaPostaci = klasaPostaci;

		sprawdzCzyJestPusty(rasa, "Rasa jest wymagana!");
		this.rasa = rasa;

		setPoziomMany(klasaPostaci);

	}

	// ------------------ METODY KOMPOZYCJI ----------------------------------------

	public static Postac utworzPostac(Gracz graczCalosc, String nazwa, Klasa klasaPostaci, Rasa rasa) throws Exception {
		if (graczCalosc == null)
			throw new Exception("Gracz nie istnieje!");

		Postac postac = new Postac(graczCalosc, nazwa, klasaPostaci, rasa);
		graczCalosc.dodajPostac(postac);

		return postac;

	}

	public Gracz getGracz() {
		return graczCalosc;
	}

	public void pokazPostac() {
		System.out.println("Postac " + nazwa + " nalezy do gracza " + graczCalosc.getNick());
	}

	// ------------------ METODY ASOCJACJI KWALIFIKOWANEJ --------------------------

	public Map<String, Przedmiot> getPrzedmiot() {
		return (Map<String, Przedmiot>) this.przedmioty.clone();
	}

	public void pokazPrzedmioty() {
		System.out.printf("Przedmioty postaci  %s :\n", nazwa);
		przedmioty.forEach((nazwa, przedmiot) -> {
			System.out.println("\t *" + przedmiot.getNazwaPrzedmiotu() + " (" + przedmiot.getPostac().nazwa + ")");
		});
		System.out.println();
	}

	public void dodajPrzedmiot(Przedmiot przedmiot) {
		if (!this.przedmioty.containsKey(przedmiot.getNazwaPrzedmiotu())) {
			this.przedmioty.put(przedmiot.getNazwaPrzedmiotu(), przedmiot);
			przedmiot.setPostac(this);
		}
	}

	public void usunPrzedmiot(Przedmiot przedmiot) {
		if (this.przedmioty.containsKey(przedmiot.getNazwaPrzedmiotu())) {
			this.przedmioty.remove(przedmiot.getNazwaPrzedmiotu());
			if (przedmiot.getPostac() == this)
				przedmiot.setPostac(null);
		}
	}

	public Przedmiot znajdzPrzedmiot(String nazwaPrzedmiotu) throws Exception {
		if (this.przedmioty.containsKey(nazwaPrzedmiotu))
			return this.przedmioty.get(nazwaPrzedmiotu);
		else
			throw new Exception("Nie odnaleziono przedmiotu o nazwie: " + nazwaPrzedmiotu);
	}

	// ------------------ OVERRIDE ------------------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Postac: " + getNazwa() + " zostala usunieta");

	}

	@Override
	public String toString() {
		return "Postac:\n " + nazwa + "\nPoziom Zdrowia: " + poziomZdrowia + " Poziom many: " + poziomMany
				+ "\nDoswiadczenie:" + doswiadczenie + "\nRasa: " + rasa + " Klasa: " + klasaPostaci + "\n";
	}

	// ------------------ GETTERS & SETTERS---------------------------

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public int getPoziomZdrowia() {
		return poziomZdrowia;
	}

	public void setPoziomZdrowia(int poziomZdrowia) {
		this.poziomZdrowia = poziomZdrowia;
	}

	public int getDoswiadczenie() {
		return doswiadczenie;
	}

	public void setDoswiadczenie(int doswiadczenie) {
		this.doswiadczenie = doswiadczenie;
	}

	public int getPoziom() {
		return poziom;
	}

	public void setPoziom(int poziom) {
		this.poziom = poziom;
	}

	public Klasa getKlasaPostaci() {
		return klasaPostaci;
	}

	public void setKlasaPostaci(Klasa klasaPostaci) {
		this.klasaPostaci = klasaPostaci;
	}

	public Rasa getRasa() {
		return rasa;
	}

	public void setRasa(Rasa rasa) {
		this.rasa = rasa;
	}

	private void setPoziomMany(Klasa klasaPostaci) {

		if (klasaPostaci == Klasa.MAGE || klasaPostaci == Klasa.MAGE || klasaPostaci == Klasa.PRIEST
				|| klasaPostaci == Klasa.WARLOCK || klasaPostaci == Klasa.SHAMAN || klasaPostaci == Klasa.DRUID)
			poziomMany = 50;
		else
			poziomMany = 0;
	}

	public int getPoziomMany() {
		return poziomMany;
	}

}
