package Overlapping;

import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Osoba extends DnDEkstensja {

	private static final long serialVersionUID = -2831920764602060574L;

	public enum TypOsoby {
		GRACZ, MISTRZ_GRY, GRACZ_MISTRZGRY
	}

	protected String imie;
	protected String nazwisko;
	protected String nick;
	protected String plec;
	protected GregorianCalendar dataUrodzenia;

	private TypOsoby typOsoby;
	private Gracz gracz;
	private MistrzGry mistrzGry;

	public Osoba(String imie, String nazwisko, String nick, String plec, GregorianCalendar dataUrodzenia,
			Gracz.Znak znak, MistrzGry.Specjalizacja spec, TypOsoby tO) throws Exception {
		super();
		inicjuj(imie, nazwisko, nick, plec, dataUrodzenia);
		this.typOsoby = tO;
		if (tO == TypOsoby.GRACZ && znak != null) {
			Gracz.stworzGracza(this, imie, nazwisko, nick, plec, dataUrodzenia, znak);
			System.out.println("Gracz " + this.getNick() + " - zostal utworzony.");
		} else if (tO == TypOsoby.MISTRZ_GRY && spec != null) {
			MistrzGry.stworzMistrzaGry(this, imie, nazwisko, nick, plec, dataUrodzenia, spec);
			System.out.println("Mistrz Gry  " + this.getNick() + " - zostal utworzony.");
		} else if (tO == TypOsoby.GRACZ_MISTRZGRY && znak != null && spec != null) {
			Gracz.stworzGracza(this, imie, nazwisko, nick, plec, dataUrodzenia, znak);
			MistrzGry.stworzMistrzaGry(this, imie, nazwisko, nick, plec, dataUrodzenia, spec);
			System.out.println("Mistrz gry, ktory jest graczem: " + this.getNick() + " - zostal utworzony.");
		}
	}

	private void inicjuj(String imie, String nazwisko, String nick, String plec, GregorianCalendar dataUrodzenia)
			throws Exception {

		sprawdzCzyJestPusty(imie, "Imie jest wymagane!");
		this.imie = imie;
		sprawdzCzyJestPusty(nazwisko, "Nazwisko jest wymagane!");
		this.nazwisko = nazwisko;
		sprawdzCzyJestPusty(nick, "Nick jest wymagany!");
		this.nick = nick;
		sprawdzCzyJestPusty(dataUrodzenia, "Data urodzenia jest wymagana!");
		this.dataUrodzenia = dataUrodzenia;

		if (this.plec == null) {
			this.plec = "Nieznana";
		} else
			this.plec = plec;
	}

	public void dodajGracza(Gracz gracz) throws Exception {
		if (this.gracz != gracz) {
			this.gracz = gracz;
			if (gracz.getOsoba() == null) {
				gracz.setOsoba(this);
			}
		}
	}

	public void dodajMistrzaGry(MistrzGry mistrzGry) throws Exception {
		if (this.mistrzGry != mistrzGry) {
			this.mistrzGry = mistrzGry;
			if (mistrzGry.getOsoba() == null) {
				mistrzGry.setOsoba(this);
			}
		}

	}

	// ------------------ OVERRIDE -----------t---------------------------

	@Override
	public String toString() {
		String str = "";
		switch(typOsoby) {
		case GRACZ:
			str = gracz.toString();
			break;
		case MISTRZ_GRY:
			str = mistrzGry.toString();
			break;
		case GRACZ_MISTRZGRY:
			str = "\n"+ gracz.toString() + "\n" + mistrzGry.toString();
			break;
		default:
				
		}
		return str;

	}

	// ------------------ GETTERS & SETTERS---------------------------

	public int getWiek() {
		return (Year.now().getValue() - dataUrodzenia.get(Calendar.YEAR));
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getDataUrodzenia() {
		return (dataUrodzenia.get(Calendar.DAY_OF_MONTH) + "/" + dataUrodzenia.get(Calendar.MONTH) + "/"
				+ dataUrodzenia.get(Calendar.YEAR));
	}

	public void setDataUrodzenia(GregorianCalendar dataUrodzenia) {
		this.dataUrodzenia = dataUrodzenia;
	}

	@Override
	public void usun() {
		UsunZEkstensji();

		if (gracz != null && mistrzGry != null) {
			gracz.usun();
			mistrzGry.usun();
		} else if (mistrzGry != null) {
			mistrzGry.usun();
		} else if (gracz != null)
			gracz.usun();
	}

}
