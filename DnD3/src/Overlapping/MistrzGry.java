package Overlapping;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import Overlapping.Gracz.Znak;

public class MistrzGry extends DnDEkstensja {

	public enum Specjalizacja {
		MISTRZ_LOCHOW, MISTRZ_ZYWIOLOW, MISTRZ_POTWOROW, MISTRZ_LEGEND
	}

	private ArrayList<String> scenariusze = new ArrayList<>();
	private ArrayList<Przygoda> przygody = new ArrayList<>();
	private Specjalizacja specjalizacja;

	private Osoba osoba;

	private MistrzGry(String imie, String nazwisko, String nick, String plec, GregorianCalendar gregorianCalendar,
			Specjalizacja spec) throws Exception {
		super();
		this.specjalizacja = spec;
	}

	public static MistrzGry stworzMistrzaGry(Osoba osoba, String imie, String nazwisko, String nick, String plec,
			GregorianCalendar gregorianCalendar, Specjalizacja spec) throws Exception {
		if (osoba == null)
			throw new Exception("Osoba nie istnieje!");

		MistrzGry mistrzGry = new MistrzGry(imie, nazwisko, nick, plec, gregorianCalendar, spec);
		osoba.dodajMistrzaGry(mistrzGry);

		return mistrzGry;
	}

	public ArrayList<String> getScenariusze() {
		return (ArrayList<String>) this.scenariusze.clone();
	}

	public ArrayList<Przygoda> getPrzygody() {
		return (ArrayList<Przygoda>) this.przygody.clone();
	}

	public void dodajScenariusz(String scenariusz) throws Exception {
		if (!scenariusze.contains(scenariusz)) {
			scenariusze.add(scenariusz);
			System.out.println("Scenariusz zostal dodany.");
		} else {
			throw new Exception("Taki scenariusz juz istnieje!");
		}
	}

	public void usunScenariusz(String scenariusz) throws Exception {
		if (!scenariusze.contains(scenariusz)) {
			scenariusze.remove(scenariusz);
			System.out.println("Scenariusz zostal usuniety.");
		} else {
			throw new Exception("Taki scenariusz nie istniejekj!");
		}
	}

	// ------------------ METODY DO ASOCJACJI BINARNEJ -----------------------

	public void dodajPrzygode(Przygoda przygoda) {
		if (!this.przygody.contains(przygoda)) {
			przygody.add(przygoda);
			if (przygoda.getMistrzGry() == null) {
				przygoda.setMistrzGry(this);
			}
		}
	}

	public void usunPrzygode(Przygoda przygoda) {
		if (this.przygody.contains(przygoda)) {
			this.przygody.remove(przygoda);
			if (przygoda.getMistrzGry() != null) {
				przygoda.setMistrzGry(null);
			}
		}
	}

	public void pokazPrzygody() {
		System.out.printf("Przygody stworzone przez Mistrza Gry, \"%s\" : \n", osoba.getNick());
		for (Przygoda p : przygody) {
			System.out.println("\t*" + p.getTytul());
		}
		System.out.println();
	}

	// --------------------------OVERRIDE-------------------------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Mistrz Gry " + osoba.getNick() + " zostal usuniety!");

	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + osoba.getNick() + " o specjalizacji: " + getSpecjalizacja();
	}

	public String getSpecjalizacja() {
		return specjalizacja.toString();
	}

	public Osoba getOsoba() {
		return osoba;
	}

	public void setOsoba(Osoba osoba) throws Exception {
		this.osoba = osoba;

		if (osoba != null) {
			osoba.dodajMistrzaGry(this);
			return;
		}

	}

}
