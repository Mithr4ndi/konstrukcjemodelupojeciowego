package Overlapping;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;

public class Gracz extends DnDEkstensja {

	public enum Znak {
		ATONACH,
		CIEN,
		CZELADNIK,
		LORD,
		RYTULAL,
		WIEZA,
		WAZ
	}
	
	private static final long serialVersionUID = 6624732662552104349L;

	private Znak znak;
	
	private ArrayList<Postac> listaPostaci = new ArrayList<>();
	private static HashSet<Postac> wszystkiePostaci = new HashSet<>();

	private ArrayList<GraczPrzygoda> rozpoczetePrzygody = new ArrayList<>();
	private ArrayList<GraczPrzygoda> zakonczonePrzygody = new ArrayList<>();

	private Osoba osoba;
	
	private Gracz(String imie, String nazwisko, String nick, String plec, GregorianCalendar gregorianCalendar, Znak znak) throws Exception {
		super();
		this.znak = znak;
	}
	

	public static Gracz stworzGracza(Osoba osoba, String imie, String nazwisko, String nick, String plec,
			GregorianCalendar gregorianCalendar, Znak znak) throws Exception {
		if (osoba == null)
			throw new Exception("Osoba nie istnieje!");

		Gracz gracz = new Gracz(imie, nazwisko, nick, plec, gregorianCalendar, znak);
		osoba.dodajGracza(gracz);

		return gracz;
	}
	
	public void pokazRozpoczetePrzygody() {
		System.out.println("Rozpoczete przygody gracza " + osoba.getNick() + ": ");
		for (GraczPrzygoda gP : rozpoczetePrzygody) {
			System.out.println(gP);
		}
	}

	public void pokazZakonczonePrzygody() {
		System.out.println("Zakonczone przygody gracza " + osoba.getNick() + ": ");
		for (GraczPrzygoda gP : zakonczonePrzygody) {
			System.out.println(gP);
		}
	}

	// ------------------ METODY KOMPOZYCJI ----------------------------------------

	public void dodajPostac(Postac postac) throws Exception {
		if (!listaPostaci.contains(postac)) {
			if (wszystkiePostaci.contains(postac)) {
				throw new Exception("Ta postac juz nalezy do innego gracza!");
			}
			listaPostaci.add(postac);
			wszystkiePostaci.add(postac);
		}
	}

	public void pokazListePostaci() {
		System.out.printf("Lista postaci gracza %s :\n", osoba.getNick());
		for (Postac p : listaPostaci) {
			System.out.println("\t *" + p.getNazwa() + " " + p.getKlasaPostaci() + " " + p.getRasa() + " ("
					+ p.getGracz().osoba.getNick() + ")");
		}
		System.out.println();
	}

	// ------------------ METODY DO ASOCJACJI Z ATRYBUTEM-------------------------
	public void rozpocznijPrzygode(Przygoda przygoda) {
		GraczPrzygoda gP = null;
		if (!GraczPrzygoda.czyIstnieje(this, przygoda)) {
			GraczPrzygoda.utworzGraczPrzygoda(this, przygoda);
		}
	}

	public void rozpocznijPrzygode(GraczPrzygoda gP) {
		if (!rozpoczetePrzygody.contains(gP)) {
			rozpoczetePrzygody.add(gP);
		}
	}

	public void zakonczPrzygode(Przygoda przygoda) {
		GraczPrzygoda result = null;
		for (GraczPrzygoda gP : rozpoczetePrzygody) {
			if (gP.getPrzygoda().equals(przygoda)) {
				result = gP;
			}
		}
		if (result != null) {

			result.aktualizujGraczPrzygoda();
			rozpoczetePrzygody.remove(result);
			if (!zakonczonePrzygody.contains(result)) {
				this.zakonczonePrzygody.add(result);
			}
		}
	}

	public ArrayList<GraczPrzygoda> getRozpoczetePrzygody() {
		return (ArrayList<GraczPrzygoda>) this.rozpoczetePrzygody.clone();
	}

	public ArrayList<GraczPrzygoda> getZakonczonePrzygody() {
		return (ArrayList<GraczPrzygoda>) this.zakonczonePrzygody.clone();
	}

	@Override
	public void usun() {
		UsunZEkstensji();
		for (Postac p : wszystkiePostaci) {
			if (p.getGracz().equals(this)) {

				p.usun();
			}
		}
		System.out.println("Gracz: " + osoba.getNick() + " zostal usuniety");
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + osoba.getNick() + " spod znaku: " + getZnak();
	}
	

	public String getZnak() {
		return znak.toString();
	}


	public Osoba getOsoba() {
		return osoba;
	}

	public void setOsoba(Osoba osoba) {
		this.osoba = osoba;
	}
	
	

	

}
