package Overlapping;

public class Helm extends Pancerz {

	private int inteligencja;
	
	
	protected Helm(Przedmiot przedmiot, int pktPancerza, int pktZdrowia, int absObrazen, int inteligencja) throws Exception {
		super(przedmiot, pktPancerza, pktZdrowia, absObrazen);
		
		sprawdzCzyJestPusty(inteligencja, "Inteligencja jest wymagana!");
		this.inteligencja = inteligencja;		
	}
	
	public int getPunktyMany(int inteligencja) {
		return (int) (inteligencja * 0.2 + inteligencja);
	}

	public int getInteligencja() {
		return inteligencja;
	}

	public void setInteligencja(int inteligencja) {
		this.inteligencja = inteligencja;
	}
	
	@Override
	public String toString() {
		return "Helm " + this.getPrzedmiot().getNazwaPrzedmiotu();
	}


}
