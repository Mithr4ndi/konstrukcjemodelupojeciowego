package Overlapping;
import java.util.Map;
import java.util.TreeMap;

public abstract class Przedmiot extends DnDEkstensja {

	public enum Rzadkosc {
		POSPOLIT, RZADKI, BARDZO_RZADKI, LEGENDARNY
	}

	public enum Rodzaj {
		BRON, PANCERZ, OZDOBA, SKŁADNIK_RZEMIESLNICZY, ZYWNOSC, ARTEFAKT, MIKSTURA
	}

	private String nazwaPrzedmiotu;
	private int poziomPrzedmiotu;
	private Rodzaj rodzajPrzemiotu;
	private Rzadkosc rzadkoscPrzedmiotu;
	private boolean czyMagiczny;

	private Postac postac;

	private Pancerz pancerz;
	

	
	public Przedmiot(String nazwaPrzedmiotu, int poziomPrzedmiotu, Rodzaj rodzajPrzedmiotu, Rzadkosc rzadkoscPrzedmiotu,
			boolean czyMagiczny) throws Exception {
		super();
		sprawdzCzyJestPusty(nazwaPrzedmiotu, "Nazwa przedmiotu jest wymagana!");
		this.nazwaPrzedmiotu = nazwaPrzedmiotu;
		
		sprawdzCzyJestPusty(poziomPrzedmiotu, "Poziom przedmiotu jest wymagany!");
		this.poziomPrzedmiotu = poziomPrzedmiotu;
		
		sprawdzCzyJestPusty(rodzajPrzedmiotu, "Rodzaj przedmiotu jets wymagany!");
		this.rodzajPrzemiotu = rodzajPrzedmiotu;
		
		sprawdzCzyJestPusty(rzadkoscPrzedmiotu, "Rzadkosc przedmiotu jest wymagana!");
		this.rzadkoscPrzedmiotu = rzadkoscPrzedmiotu;
		
		sprawdzCzyJestPusty(czyMagiczny, "Informacja o tym czy przedmiot jest magiczny jest wymagana!");
		this.czyMagiczny = czyMagiczny;
	}
	
	
	
	// ------------------ METODY KOMPOZYCJI ----------------------------------------
	
	public void dodajPancerz(Pancerz pancerz) throws Exception {
		if (pancerz == null || this.pancerz != pancerz) {
			this.pancerz = pancerz;
			}
	}
	

	// ------------------ METODY ASOCJACJI KWALIFIKOWANEJ ---------------------------

	public Postac getPostac(){
		return this.postac;
	}
	
	public void setPostac(Postac postac) {
		if(this.postac == null){
			postac.dodajPrzedmiot(this);
			this.postac = postac;
		}
		else if(postac == null){
			this.postac.usunPrzedmiot(this);
			this.postac = null;
		}
		else if(this.postac != postac){
			this.postac.usunPrzedmiot(this);
			postac.dodajPrzedmiot(this);
			this.postac = postac;
		}
	}
	
	
	// ------------------ OVERRIDE ---------------------------------------------------
	
	@Override
	public void usun() {
		UsunZEkstensji();
		if(pancerz.equals(this)) {
			pancerz.usun();
		}
		System.out.println("Przedmiot: " + getNazwaPrzedmiotu() + " zostal usuniety.");
	}
	
	@Override
	public String toString() {
		return "Przedmiot:\n" + nazwaPrzedmiotu + "\nPoziom: " + poziomPrzedmiotu + "\nRodzaj " + rodzajPrzemiotu.toString() +"\n"
				+ (isCzyMagiczny() == true ? "Magiczny" : "Niemagiczny") +"\n";
	}
	
	
	// ------------------ GETTERS & SETTERS---------------------------
	
	public String getNazwaPrzedmiotu() {
		return nazwaPrzedmiotu;
	}

	public void setNazwa(String nazwaPrzedmiotu) {
		this.nazwaPrzedmiotu = nazwaPrzedmiotu;
	}

	public int getPoziomPrzedmiotu() {
		return poziomPrzedmiotu;
	}

	public void setPoziomPrzedmiotu(int poziomPrzedmiotu) {
		this.poziomPrzedmiotu = poziomPrzedmiotu;
	}

	public Rodzaj getRodzajPrzemiotu() {
		return rodzajPrzemiotu;
	}

	public void setRodzajPrzemiotu(Rodzaj rodzajPrzemiotu) {
		this.rodzajPrzemiotu = rodzajPrzemiotu;
	}

	public Rzadkosc getRzadkoscPrzedmiotu() {
		return rzadkoscPrzedmiotu;
	}

	public void setRzadkoscPrzedmiotu(Rzadkosc rzadkoscPrzedmiotu) {
		this.rzadkoscPrzedmiotu = rzadkoscPrzedmiotu;
	}

	public boolean isCzyMagiczny() {
		return czyMagiczny;
	}

	public void setCzyMagiczny(boolean czyMagiczny) {
		this.czyMagiczny = czyMagiczny;
	}

}
