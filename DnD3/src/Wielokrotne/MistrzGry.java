package Wielokrotne;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MistrzGry extends Osoba implements IMistrzGry {

	public enum Specjalizacja {
			MISTRZ_LOCHOW,
			MISTRZ_ZYWIOLOW,
			MISTRZ_POTWOROW,
			MISTRZ_LEGEND		
	}
	
	private ArrayList<String> scenariusze = new ArrayList<>();
	private ArrayList<Przygoda> przygody = new ArrayList<>();
	private Specjalizacja specjalizacja;

	public MistrzGry(String imie, String nazwisko, String nick, String plec, GregorianCalendar gregorianCalendar, Specjalizacja spec)
			throws Exception {
		super(imie, nazwisko, nick, plec, gregorianCalendar);
		this.specjalizacja = spec;
	}

	public MistrzGry(Osoba osoba, Specjalizacja spec) throws Exception {
		super(osoba.imie, osoba.nazwisko, osoba.nick, osoba.plec, osoba.dataUrodzenia);
		this.specjalizacja = spec;
		System.out.println(getNick() + " jest teraz Mistrzem Gry!");
	}
	
	
	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#getScenariusze()
	 */
	@Override
	public ArrayList<String> getScenariusze() {
		return (ArrayList<String>) this.scenariusze.clone();
	}
	
	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#getPrzygody()
	 */
	@Override
	public ArrayList<Przygoda> getPrzygody() {
		return (ArrayList<Przygoda>) this.przygody.clone();
	}

	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#dodajScenariusz(java.lang.String)
	 */
	@Override
	public void dodajScenariusz(String scenariusz) throws Exception {
		if (!scenariusze.contains(scenariusz)) {
			scenariusze.add(scenariusz);
			System.out.println("Scenariusz zostal dodany.");
		} else {
			throw new Exception("Taki scenariusz juz istnieje!");
		}
	}

	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#usunScenariusz(java.lang.String)
	 */
	@Override
	public void usunScenariusz(String scenariusz) throws Exception {
		if (!scenariusze.contains(scenariusz)) {
			scenariusze.remove(scenariusz);
			System.out.println("Scenariusz zostal usuniety.");
		} else {
			throw new Exception("Taki scenariusz nie istniejekj!");
		}
	}
	
	

	// ------------------ METODY DO ASOCJACJI BINARNEJ -----------------------

	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#dodajPrzygode(Wielokrotne.Przygoda)
	 */
	@Override
	public void dodajPrzygode(Przygoda przygoda) {
		if (!this.przygody.contains(przygoda)) {
			przygody.add(przygoda);
			if (przygoda.getMistrzGry() == null) {
				przygoda.setMistrzGry(this);
			}
		}
	}

	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#usunPrzygode(Wielokrotne.Przygoda)
	 */
	@Override
	public void usunPrzygode(Przygoda przygoda) {
		if (this.przygody.contains(przygoda)) {
			this.przygody.remove(przygoda);
			if (przygoda.getMistrzGry() != null) {
				przygoda.setMistrzGry(null);
			}
		}
	}

	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#pokazPrzygody()
	 */
	@Override
	public void pokazPrzygody() {
		System.out.printf("Przygody stworzone przez Mistrza Gry, \"%s\" : \n",getNick());
		for (Przygoda p : przygody) {
			System.out.println("\t*" + p.getTytul());
		}
		System.out.println();
	}

	// --------------------------OVERRIDE-------------------------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Mistrz Gry " + getNick() + " zostal usuniety!");

	}

	/* (non-Javadoc)
	 * @see Wielokrotne.IMistrzGry#getWlasciwosc()
	 */
	@Override
	public String getWlasciwosc() {
		return specjalizacja.toString();
	}
		
}
