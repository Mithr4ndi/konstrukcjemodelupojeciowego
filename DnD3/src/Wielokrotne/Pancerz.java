package Wielokrotne;

public class Pancerz extends DnDEkstensja{


	private static final long serialVersionUID = 1L;
	
	protected int punktyPancerza;
	protected int punktyZdrowia;
	protected int absorpcjaObrazen;
	
	protected Przedmiot przedmiot;
	
	protected Pancerz(Przedmiot przedmiot, int pktPancerza, int pktZdrowia, int absObrazen) {
		super();
		this.punktyPancerza = pktPancerza;
		this.punktyZdrowia = pktZdrowia;
		this.absorpcjaObrazen = absObrazen;

	}
	
	// ------------------ METODY KOMPOZYCJI ----------------------------------------
	public static Pancerz utworzPancerz(Przedmiot przedmiot, int pktPancerza, int pktZdrowia, int absObrazen) throws Exception {
		if (przedmiot == null)
			throw new Exception("Przedmiot nie istnieje!");

		Pancerz pancerz = new Pancerz(przedmiot, pktPancerza, pktZdrowia, absObrazen);
		przedmiot.dodajPancerz(pancerz);

		return pancerz;
	}

	public Przedmiot getPrzedmiot() {
		return przedmiot;
	}
	
	public void pokazPrzedmiot() {
		System.out.println("Przedmiot " + przedmiot.getNazwaPrzedmiotu() + " to pancerz.");
	}
	
	// ------------------ OVERRIDE ------------------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Pancerz zostal usuniety!");
	}
	
	// ------------------ GETTERS & SETTERS---------------------------
	
	public int getPunktyPancerza() {
		return punktyPancerza;
	}

	public void setPunktyPancerza(int punktyPancerza) {
		this.punktyPancerza = punktyPancerza;
	}

	public int getPunktyZdrowia() {
		return punktyZdrowia;
	}

	public void setPunktyZdrowia(int punktyZdrowia) {
		this.punktyZdrowia = punktyZdrowia;
	}

	public int getAbsorpcjaObrazen() {
		return absorpcjaObrazen;
	}

	public void setAbsorpcjaObrazen(int absorpcjaObrazen) {
		this.absorpcjaObrazen = absorpcjaObrazen;
	}

}
