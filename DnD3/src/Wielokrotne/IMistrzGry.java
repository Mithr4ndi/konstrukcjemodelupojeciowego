package Wielokrotne;

import java.util.ArrayList;

public interface IMistrzGry {

	ArrayList<String> getScenariusze();

	ArrayList<Przygoda> getPrzygody();

	void dodajScenariusz(String scenariusz) throws Exception;

	void usunScenariusz(String scenariusz) throws Exception;

	void dodajPrzygode(Przygoda przygoda);

	void usunPrzygode(Przygoda przygoda);

	void pokazPrzygody();

	String getWlasciwosc();

}