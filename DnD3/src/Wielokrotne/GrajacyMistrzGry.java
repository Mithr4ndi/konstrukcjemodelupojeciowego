package Wielokrotne;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class GrajacyMistrzGry extends Gracz implements IMistrzGry{

	MistrzGry mistrzGry;
	
	public GrajacyMistrzGry(String imie, String nazwisko, String nick, String plec, GregorianCalendar gregorianCalendar,
			Znak znak, MistrzGry.Specjalizacja spec) throws Exception {
		super(imie, nazwisko, nick, plec, gregorianCalendar, znak);
		
		mistrzGry = new MistrzGry(null, null, null, null, null, spec);
		
	}

	@Override
	public ArrayList<String> getScenariusze() {
		return mistrzGry.getScenariusze();
	}

	@Override
	public ArrayList<Przygoda> getPrzygody() {
		return mistrzGry.getPrzygody();
	}

	@Override
	public void dodajScenariusz(String scenariusz) throws Exception {
		mistrzGry.dodajScenariusz(scenariusz);
	}

	@Override
	public void usunScenariusz(String scenariusz) throws Exception {
		mistrzGry.usunScenariusz(scenariusz);
	}

	@Override
	public void dodajPrzygode(Przygoda przygoda) {
		mistrzGry.dodajPrzygode(przygoda);
	}

	@Override
	public void usunPrzygode(Przygoda przygoda) {
		mistrzGry.usunPrzygode(przygoda);
	}

	@Override
	public void pokazPrzygody() {
		mistrzGry.pokazPrzygody();
	}
	
	@Override
	public String getWlasciwosc() {
		return "\nSpecjalizacja Mistrza Gry: " + mistrzGry.getWlasciwosc() + "\nZnak gracza: " + super.getWlasciwosc();
	}

}
