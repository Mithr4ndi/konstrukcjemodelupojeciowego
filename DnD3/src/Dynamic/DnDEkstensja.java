package Dynamic;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class DnDEkstensja implements Serializable {

	private static final long serialVersionUID = 2065406251119175749L;

	// Atrybut klasowy
	private static HashMap<Class, ArrayList<DnDEkstensja>> wszystkieEkstensje = new HashMap();

	public DnDEkstensja() {
		ArrayList<DnDEkstensja> pojedynczaEkstensja = null;
		Class klasa = this.getClass();

		if (wszystkieEkstensje.containsKey(klasa)) {
			pojedynczaEkstensja = wszystkieEkstensje.get(klasa);
		} else {
			pojedynczaEkstensja = new ArrayList<DnDEkstensja>();
			wszystkieEkstensje.put(klasa, pojedynczaEkstensja);
		}

		pojedynczaEkstensja.add(this);
	}

	public abstract void usun();

	protected void UsunZEkstensji() {
		ArrayList<DnDEkstensja> pojedynczaEkstensja = null;
		Class klasa = this.getClass();

		if (wszystkieEkstensje.containsKey(klasa)) {
			pojedynczaEkstensja = wszystkieEkstensje.get(klasa);
			pojedynczaEkstensja.remove(this);
		}

	}

	// Przeciazenie metody UsunZEkstensji
	public static void UsunZEkstensji(DnDEkstensja obiekt) {
		ArrayList<DnDEkstensja> pojedynczaEkstensja = null;
		Class klasa = obiekt.getClass();

		if (wszystkieEkstensje.containsKey(klasa)) {
			pojedynczaEkstensja = wszystkieEkstensje.get(klasa);
			pojedynczaEkstensja.remove(obiekt);
		}
	}

	public void sprawdzCzyJestPusty(Object o, String message) throws Exception {
		if (o == null) {
			UsunZEkstensji();
			throw new Exception(message);
		}
	}

	public static void zapiszEkstensje(ObjectOutputStream stream) throws IOException {
		stream.writeObject(wszystkieEkstensje);
	}

	public static void odczytajEkstensje(ObjectInputStream stream) throws ClassNotFoundException, IOException {
		wszystkieEkstensje = (HashMap<Class, ArrayList<DnDEkstensja>>) stream.readObject();
	}

	public static void pokazEkstensje(Class klasa) throws Exception {
		ArrayList<DnDEkstensja> pojedynczaEkstensja = getEkstensja(klasa);

		System.out.println("Ekstensja klasy: " + klasa.getSimpleName());
		for (Object obiekt : pojedynczaEkstensja) {
			System.out.println(obiekt);
		}
	}

	// Metoda klasowa
	public static ArrayList<DnDEkstensja> getEkstensja(Class klasa) throws Exception {
		ArrayList<DnDEkstensja> pojedynczaEkstensja = null;
		if (wszystkieEkstensje.containsKey(klasa)) {
			pojedynczaEkstensja = wszystkieEkstensje.get(klasa);
		} else {
			throw new Exception("Nieznana klasa " + klasa);
		}
		return pojedynczaEkstensja;
	}

}
