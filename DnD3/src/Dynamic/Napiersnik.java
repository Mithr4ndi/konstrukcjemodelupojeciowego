package Dynamic;

public class Napiersnik extends Pancerz {

	private int wytrzymalosc;
	
	protected Napiersnik(Przedmiot przedmiot, int pktPancerza, int pktZdrowia, int absObrazen, int wytrzymalosc) throws Exception {
		super(przedmiot, pktPancerza, pktZdrowia, absObrazen);
		
		sprawdzCzyJestPusty(wytrzymalosc, "Wytrzymalosc jest wymagana!");
		this.wytrzymalosc = wytrzymalosc;
	}

	public int getWytrzymalosc() {
		return wytrzymalosc;
	}

	public void setWytrzymalosc(int wytrzymalosc) {
		this.wytrzymalosc = wytrzymalosc;
	}

	public int getPunktyZdrowia(int wytrzymalosc) {
		return (int) (wytrzymalosc * 0.2 + wytrzymalosc);
	}
	
	@Override
	public String toString() {
		return "Napiersnik " + this.getPrzedmiot().getNazwaPrzedmiotu();
	}
}
