package Dynamic;
import java.util.ArrayList;

public class Lokacja extends DnDEkstensja {

	private String nazwaLokacji;
	private int poziom;
	private String historia;
	private ArrayList<String> zadania = new ArrayList<>();

	// Asocjacja binarna
	private Przygoda przygoda;

	public Lokacja(String nazwaLokacji, int poziom, String historia) throws Exception {
		super();
		sprawdzCzyJestPusty(nazwaLokacji, "Nazwa lokacji jest wymagana!");
		this.nazwaLokacji = nazwaLokacji;

		sprawdzCzyJestPusty(poziom, "Poziom jest wymagany!");
		this.poziom = poziom;

		if (historia == null) {
			this.historia = "Jeszcze nikt nie spisal historii tego miejsca...";
		} else {
			this.historia = historia;
		}
	}

	// ------------------ METODY DO ASOCJACJI BINARNEJ -----------------------

	public Przygoda getPrzygoda() {
		return przygoda;
	}

	public void setPrzygoda(Przygoda przygoda) {
		this.przygoda = przygoda;

		if (przygoda != null) {
			przygoda.dodajLokacje(this);
			return;
		}
	}
	
	public void pokazPrzygode() {
		System.out.println("Lokacja znajduje sie w przygodzie: " + przygoda.getTytul() + "\n");
	}

	// ------------------ OVERRIDE ---------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Lokacja: " + getNazwaLokacji() + " zostala usunieta");
	}

	@Override
	public String toString() {
		return "Lokacja:\n" + nazwaLokacji + "\nPoziom: " + poziom + "\nPrzygoda: "
				+ (przygoda == null ? "Nie przypisano przygody" : przygoda);
	}

	public void dodajZadanie(String zadanie) {
		zadania.add(zadanie);
		System.out.println("Zadanie zostalo dodane.");
	}

	// ------------------ GETTERS & SETTERS---------------------------

	public String getNazwaLokacji() {
		return nazwaLokacji;
	}

	public void setNazwaLokacji(String nazwaLokacji) {
		this.nazwaLokacji = nazwaLokacji;
	}

	public int getPoziom() {
		return poziom;
	}

	public void setPoziom(int poziom) {
		this.poziom = poziom;
	}

	public String getHistoria() {
		return historia;
	}

	public void setHistoria(String historia) {
		this.historia = historia;
	}

	public ArrayList<String> getZadania() {
		return zadania;
	}

}
