package Dynamic;

import java.util.ArrayList;

public class Przygoda extends DnDEkstensja {

	public enum PoziomTrudnosci {
		LATWY, NORMALNY, TRUDNY
	};

	private String tytul;
	private PoziomTrudnosci poziomTrudnosci;
	private int liczbaGraczy;
	private int liczbaLokacji;
	private String fabula;

	// Asocjacja z atrybutem
	private ArrayList<GraczPrzygoda> gracze = new ArrayList<>();

	// Asocjacja binarna
	private ArrayList<Lokacja> lokacje = new ArrayList<>();
	private MistrzGry mistrzGry;
	
	

	private Przygoda(String tytul, PoziomTrudnosci pT, int lG, int lL, String fabula) throws Exception {
		super();
		sprawdzCzyJestPusty(tytul, "Tytul jest wymagany!");
		this.tytul = tytul;

		sprawdzCzyJestPusty(pT, "Poziom Trudnosci jest wymagany!");
		this.poziomTrudnosci = pT;

		if (fabula == null) {
			fabula = "Brak";
		} else {
			this.fabula = fabula;
		}

		if (lG >= 3) {
			this.liczbaGraczy = lG;
		} else {
			throw new Exception("Minimalna liczba graczy to 3!");
		}

		if (lL >= 2) {
			this.liczbaLokacji = lL;
		} else {
			throw new Exception("Minimalna liczba lokacji to 2!");
		}

	}

	//----------------------------------------------------------------------------------------------
	public static Przygoda stworzPrzygode(MistrzGry mistrzGry, String tytul, PoziomTrudnosci pT, int lG, int lL, String fabula) throws Exception {
		if (mistrzGry == null)
			throw new Exception("Mistrz Gry nie istnieje!");

		Przygoda przygoda = new Przygoda(tytul, pT, lG, lL, fabula);
		mistrzGry.dodajPrzygode(przygoda);

		return przygoda;
	}
	
	public MistrzGry getMistrzGry() {
		return mistrzGry;
	}

	public void setMistrzGry(MistrzGry mistrzGry) {
		this.mistrzGry = mistrzGry;

		if (mistrzGry != null) {
			mistrzGry.dodajPrzygode(this);
			return;
		}
	}
	
	public void pokazPrzygode() {
		System.out.println("Przygoda stworzona przez: " + mistrzGry.getNick() + "\n");
	}

		
	// ------------------ METODY DO ASOCJACJI BINARNEJ -----------------------

	public void dodajLokacje(Lokacja lokacja) {
		if (!this.lokacje.contains(lokacja)) {
			lokacje.add(lokacja);
			if (lokacja.getPrzygoda() == null) {
				lokacja.setPrzygoda(this);
			}

		}
	}

	public void usunLokacje(Lokacja lokacja) {
		if (this.lokacje.contains(lokacja)) {
			this.lokacje.remove(lokacja);
			if (lokacja.getPrzygoda() != null) {
				lokacja.setPrzygoda(null);
			}
		}
	}

	public void pokazLokacje() {
		System.out.printf("Lokacje, w przygodzie \"%s\" : \n", tytul);
		for (Lokacja l : lokacje) {
			System.out.println("\t*" + l.getNazwaLokacji());
		}
		System.out.println();
	}

	// ------------------ METODY DO ASOCJACJI Z ATRYBUTEM --------------------
	public void dodajGracza(Gracz gracz) {
		GraczPrzygoda gP = null;
		if (!GraczPrzygoda.czyIstnieje(gracz, this)) {
			GraczPrzygoda.utworzGraczPrzygoda(gracz, this);
		}		
	}
	
	public void dodajGracza(GraczPrzygoda gP){
		if(!gracze.contains(gP)) {
			gracze.add(gP);
		}
	}

	public ArrayList<Gracz> getGracze() {
		ArrayList<Gracz> tmpGracz = new ArrayList<>();
		for (GraczPrzygoda gP : this.gracze) {
			tmpGracz.add(gP.getGracz());
		}
		return tmpGracz;
	}

	public void pokazGraczy() {
		System.out.printf("Gracze bioracy udzial w przygodzie %s :\n", tytul);
		for (GraczPrzygoda gP : gracze) {
			System.out.println("\t *" + gP.getGracz().getNick());
		}
		System.out.println();
	}

	// ------------------ OVERRIDE ---------------------------

	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Przygoda: " + getTytul() + " zostala usunieta");
	}

	@Override
	public String toString() {
		return "Przygoda:\n " + tytul + "\n" + poziomTrudnosci + "\nLiczba graczy: " + liczbaGraczy
				+ "\nLiczbalokacji: " + liczbaLokacji + "\n";
	}

	// ------------------ GETTERS & SETTERS---------------------------

	public String getTytul() {
		return tytul;
	}

	public void setTytul(String tytul) {
		this.tytul = tytul;
	}

	public PoziomTrudnosci getPoziomTrudnosci() {
		return poziomTrudnosci;
	}

	public void setPoziomTrudnosci(PoziomTrudnosci poziomTrudnosci) {
		this.poziomTrudnosci = poziomTrudnosci;
	}

	public int getLiczbaGraczy() {
		return liczbaGraczy;
	}

	public void setLiczbaGraczy(int liczbaGraczy) {
		this.liczbaGraczy = liczbaGraczy;
	}

	public int getLiczbaLokacji() {
		return liczbaLokacji;
	}

	public void setLiczbaLokacji(int liczbaLokacji) {
		this.liczbaLokacji = liczbaLokacji;
	}

	public String getFabula() {
		return fabula;
	}

	public void setFabula(String fabula) {
		this.fabula = fabula;
	}

}
