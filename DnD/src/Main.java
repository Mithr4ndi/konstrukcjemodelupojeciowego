import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectInputStream.GetField;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class Main {

	public static void main(String[] args) throws Exception{

	
		Gracz gracz1 = new Gracz("Jan","Kowalski", "Pikus", new GregorianCalendar(1995, 2, 11), null );
		InicjujObiektGracz(gracz1);	
		Gracz gracz2 = new Gracz("Anna","Malina", "Malinka", new GregorianCalendar(1990, 4, 10), null );
		InicjujObiektGracz(gracz2);	
		Gracz gracz3 = new Gracz("Stefan","Krawczyk", "Krawiec", new GregorianCalendar(1992, 5, 9), null );
		InicjujObiektGracz(gracz3);	
		
		try {
		Gracz gracz4 = new Gracz(null,"Krawczyk", "Krawiec", new GregorianCalendar(1992, 5, 9), null );
		}catch(Exception e) {
			System.err.println(e.getMessage());
		}
		System.out.println();
		

		System.out.println("Atrybut Powtarzalny: ");
		System.out.println("Lista Rozpoczetych Przygod gracza " + gracz1.getNick() + ": ");
		for(String str : gracz1.rozpoczetePrzygody) {
			System.out.println(str);
		}
		System.out.println();
		
		System.out.println("Atrybut Wyliczalny: ");
		System.out.println("Wiek gracza " +gracz3.getNick() + ": " + gracz3.getWiek());
		System.out.println();
			
			DnDEkstensja.zapiszEkstensje(new ObjectOutputStream(new FileOutputStream("DnDEkstensja.txt")));
			DnDEkstensja.odczytajEkstensje(new ObjectInputStream(new FileInputStream("DnDEkstensja.txt")));
			Gracz.pokazEkstensje(Gracz.class);
			System.out.println();
			
			ArrayList<DnDEkstensja> lista = DnDEkstensja.getEkstensja(Gracz.class);
			DnDEkstensja.UsunZEkstensji(lista.get(0));
			
			Gracz.pokazEkstensje(Gracz.class);
			System.out.println();

	}

	static String[] nazwyPrzygod = { "Bitwa o Undercity", "Upadek Angratharu", "Klatwa Ravenlorda", "Plonaca Krucjata",
			"Ucieczka z Durnholde", "Powr�t do Teldrassil", "Odrodzone Przymierze", "Pani Lodowej Cytadeli",
			"Ostatnia Jask�ka", "Kwiat Wisielc�w", "Ogr�d �mierci", "Krew smoka" };

	public static void InicjujObiektGracz(Gracz g) {
		Random rand = new Random();

		for (int i = 0; i < 3; i++) {
			g.dodajRozpoczetaPrzygode(nazwyPrzygod[rand.nextInt(nazwyPrzygod.length)]);
		}

		for (int i = 0; i < 5; i++) {
			g.dodajZakonczonaPrzygode(nazwyPrzygod[rand.nextInt(nazwyPrzygod.length)]);
		}
	}

}
