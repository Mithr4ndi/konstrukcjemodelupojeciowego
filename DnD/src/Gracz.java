import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class Gracz extends Osoba {


	private static final long serialVersionUID = 6624732662552104349L;
	
	ArrayList<String> listaPostaci = new ArrayList<>();
	ArrayList<String> rozpoczetePrzygody = new ArrayList<>();
	ArrayList<String> zakonczonePrzygody = new ArrayList<>();
	

	public Gracz(String imie, String nazwisko, String nick, GregorianCalendar gregorianCalendar, String plec) throws Exception {
		super(imie, nazwisko, nick, plec, gregorianCalendar);
	}

	public void dodajRozpoczetaPrzygode(String przygoda) {
		rozpoczetePrzygody.add(przygoda);
	}
	
	public void dodajZakonczonaPrzygode(String przygoda) {
		zakonczonePrzygody.add(przygoda);
	}

	//Przesloniecie metody abstrakcyjnej usun z klasy DnDEkstensja
	@Override
	public void usun() {
		UsunZEkstensji();
		System.out.println("Gracz: " + getNick() + " zostal usuniety");
	}
		
}
